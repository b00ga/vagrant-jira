node default {
  package { 'elinks':
    ensure => latest,
  }

  include ntp
  include apache
  include apache::mod::php
  include postgresql::server
  postgresql::server::db { 'jiradb':
    user => 'jira',
    password => postgresql_password('jira','jira'),
  }
  include phppgadmin

  archive { 'atlassian-jira-6.2':
    ensure => present,
    url => 'http://downloads.atlassian.com/software/jira/downloads/atlassian-jira-6.2.tar.gz',
    # From URL http://downloads.atlassian.com/software/jira/downloads/atlassian-jira-6.2.tar.gz.md5
    digest_string => '71a521cd983bc0892dc64027456cea25',
    digest_type => 'md5',
    target => '/opt/atlassian',
    root_dir => 'atlassian-jira-6.2-standalone',
  }
  file { '/opt/atlassian/atlassian-jira-6.2-standalone/logs':
    ensure => directory,
    owner => 'jira',
    group => 'jira',
    require => [ User['jira'], Group['jira'], Archive['atlassian-jira-6.2'] ],
  }
  file { '/opt/atlassian/atlassian-jira-6.2-standalone/work':
    ensure => directory,
    owner => 'jira',
    group => 'jira',
    require => [ User['jira'], Group['jira'], Archive['atlassian-jira-6.2'] ],
  }
  file { '/opt/atlassian/atlassian-jira-6.2-standalone/temp':
    ensure => directory,
    owner => 'jira',
    group => 'jira',
    require => [ User['jira'], Group['jira'], Archive['atlassian-jira-6.2'] ],
  }
  file { '/opt/atlassian/jira':
    ensure => link,
    target => '/opt/atlassian/atlassian-jira-6.2-standalone',
    require => Archive['atlassian-jira-6.2'],
  }
  user { 'jira':
    ensure => present,
    gid => 'jira',
    system => true,
    home => '/srv/jira-home',
    managehome => true,
    shell => '/bin/bash',
    require => Group['jira'],
  }
  group { 'jira':
    ensure => present,
  }
  file { '/srv/jira-home':
    ensure => directory,
    owner => 'jira',
    group => 'jira',
    require => [ User['jira'], Group['jira'] ],
  }
  file { 'jira-application.properties':
    ensure => file,
    path    => '/opt/atlassian/jira/atlassian-jira/WEB-INF/classes/jira-application.properties',
    content => 'jira.home = /srv/jira-home',
    require => Archive['atlassian-jira-6.2'],
  }

  include apt
  apt::ppa { 'ppa:webupd8team/java': }
  file { '/var/cache/debconf/oracle-java7-installer.preseed':
    ensure => file,
    mode => 600,
    content => 'oracle-java7-installer shared/accepted-oracle-license-v1-1 select true',
  }
  package { 'oracle-java7-installer':
    ensure => latest,
    responsefile => '/var/cache/debconf/oracle-java7-installer.preseed',
    require => Apt::Ppa['ppa:webupd8team/java'],
  }

  file { '/srv/jira-home/dbconfig.xml':
    ensure => file,
    owner => 'jira',
    group => 'jira',
    source => 'file:///vagrant/files/dbconfig.xml',
    require => File['/srv/jira-home'],
  }
  file { '/srv/jira-home/import':
    ensure => directory,
    owner => 'jira',
    group => 'jira',
  }
  file { '/srv/jira-home/import/startupdatabase.xml':
    ensure => file,
    owner => 'jira',
    group => 'jira',
    source => 'file:///opt/atlassian/atlassian-jira-6.2-standalone/atlassian-jira/WEB-INF/classes/startupdatabase.xml',
    require => [ File['/srv/jira-home'], Archive['atlassian-jira-6.2'] ],
  }

}
